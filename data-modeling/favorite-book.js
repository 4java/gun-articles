module.exports = function(deps) {
  const {uuid, log} = deps;

  function favoriteBooks(opt) {
    const {db, reader, books, listName, isPublic} = opt;
    const listId = uuid();

    const list = db.get(listId).put({
      uuid: listId,
      type: "Link",
      name: "favorite_list",
      list_name: listName,
      is_public: isPublic,
    });

    const faveBooks = db.get(uuid());
    for (book of books) {
      faveBooks.set(book);
    }

    list.get("books").put(faveBooks);
    list.get("belongs_to").put(reader);

    reader.get("favorite_books").set(list);

    return list;
  }

  function createFavoriteLists(opt) {
    const {db, readerNodes, bookNodes} = opt;

    /* Reader 1 creates two lists of favorite books */
    favoriteBooks({
      db,
      reader: readerNodes[0],
      books: [bookNodes[0], bookNodes[1]],
      listName: "List 1",
      isPublic: "false",
    });

    favoriteBooks({
      db,
      reader: readerNodes[0],
      books: [bookNodes[1], bookNodes[2], bookNodes[3]],
      listName: "List 2",
      isPublic: "true",
    });

  }

  function favoriteBooksQueries(opt) {
    const {db, readerNodes, bookNodes} = opt;
    const queries = {

    1: () => {
      /* log all the favorite books for reader 1 across all of their lists */
      readerNodes[0].get("favorite_books").map().get("books").map().get("title").once(log);
    },

    2:() => {
      /* log the titles of the favorite books for reader 1 in the "List 1" list */
      readerNodes[0].get("favorite_books").map().once(list => {
        if(list.list_name === "List 1") {
          db.get(list.books).map().get("title").once(log);
        }
      });
    },

    3:() => {
      /* log the names of the private lists created by reader 1 */
      readerNodes[0].get("favorite_books").map().once(list => {
        if(list.is_public === "false") {
          log(list.list_name);
        }
      });
    },

    };

    return queries;
  }

  function favoriteBooksQueriesPromised(opt) {
    const {db, readerNodes, bookNodes} = opt;

    const filterMetadata = (o) => {
      const copy = {...o};
      delete copy._;
      return copy;
    };

    const queries = {
      1: () => {

      },

    };

    return queries;
  }

  return {
    createNodes: createFavoriteLists,
    queries: favoriteBooksQueries,
    queriesPromised: favoriteBooksQueriesPromised,
  }
};


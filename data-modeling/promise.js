const filterMetadata = (o) => {
  const copy = {...o};
  delete copy._;
  return copy;
};

const nodes = (opt) => {
  const {db, query} = opt;
  return query.then().then(filterMetadata)
  .then(r => Promise.all(Object.keys(r).map(k => db.get(k).then())));
};

const getAt = (db, key) => (r) => {
  return Promise.all(Object.keys(r).map(k => {
    if(r[k][key]) {
      return db.get(r[k][key]["#"]).then();
    }
    return {};
  }))
  .then(r => r.map(filterMetadata));
};

module.exports = {
  filterMetadata,
  dbNodes: nodes,
  getAt,
};

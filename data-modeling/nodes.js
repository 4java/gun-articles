module.exports = function(deps) {
  const {fakers} = deps;

  function create(opt) {
    const {db} = opt;

    /* Generate fake data in plain JavaScript */
    const users = fakers.fakeUsers();
    const roles = fakers.fakeRoles();
    const books = fakers.fakeBooks();
    const categories = fakers.fakeCategories();
    const readers = fakers.fakeReaders();
    const authors = fakers.fakeAuthors();
    const publishers = fakers.fakePublishers();

    /* Add data to Gun */
    for (let u of users) {
      db.get(u.uuid).put(u);
    }
    for (let r of roles) {
      db.get(r.uuid).put(r);
    }
    for (let b of books) {
      db.get(b.uuid).put(b);
    }
    for (let c of categories) {
      db.get(c.uuid).put(c);
    }
    for (let r of readers) {
      db.get(r.uuid).put(r);
    }
    for (let a of authors) {
      db.get(a.uuid).put(a);
    }
    for (let p of publishers) {
      db.get(p.uuid).put(p);
    }

    /* Get a reference to nodes from Gun */
    const userNodes = users.map(n => db.get(n.uuid));
    const roleNodes = roles.map(n => db.get(n.uuid));
    const bookNodes = books.map(n => db.get(n.uuid));
    const categoryNodes = categories.map(n => db.get(n.uuid));
    const readerNodes = readers.map(n => db.get(n.uuid));
    const authorNodes = authors.map(n => db.get(n.uuid));
    const publisherNodes = publishers.map(n => db.get(n.uuid));

    return {
      userNodes, roleNodes, bookNodes, categoryNodes,
      readerNodes, authorNodes,
      publisherNodes,
    };
  }

  return {
    create,
  };
};

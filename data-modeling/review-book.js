const {dbNodes, getAt, filterMetadata} = require("./promise");
module.exports = function(deps) {
  const {uuid, log} = deps;

  function reviewBook(opt) {
    const {db, reader, book, rating, content} = opt;
    const linkId = uuid();

    const review = db.get(linkId).put({
      uuid: linkId,
      type: "Link",
      name: "review_book",
      rating: rating,
      content: content,
    });
    review.get("book").put(book);
    review.get("reader").put(reader);
    db.get(`reviews/${rating}`).set(review);

    book.get("reviews").set(review);
    reader.get("book_reviews").set(review);

    return review;
  }

  function createReviewBookNodes(opt) {
    const {db, readerNodes, bookNodes} = opt;

    /* Reader 1 reviews books */
    reviewBook({
      db,
      reader: readerNodes[0],
      book: bookNodes[0],
      rating: 5,
      content: "Great book!",
    });

    reviewBook({
      db,
      reader: readerNodes[0],
      book: bookNodes[1],
      rating: 1,
      content: "It was ok.",
    });

    reviewBook({
      db,
      reader: readerNodes[0],
      book: bookNodes[2],
      rating: 2,
      content: "Not bad.",
    });

    /* Reader 2 reviews books */
    reviewBook({
      db,
      reader: readerNodes[1],
      book: bookNodes[0],
      rating: 5,
      content: "Fantastic book!",
    });

    reviewBook({
      db,
      reader: readerNodes[1],
      book: bookNodes[1],
      rating: 5,
      content: "Amazing book!",
    });

    reviewBook({
      db,
      reader: readerNodes[1],
      book: bookNodes[2],
      rating: 1,
      content: "Not bad.",
    });

    /* Reader 3 reviews books */
    reviewBook({
      db, reader: readerNodes[2],
      book: bookNodes[0],
      rating: 1,
      content: "lorem ipsum"
    });
  }

  function reviewBookQueries(opt) {
    const {db, readerNodes, bookNodes} = opt;
    const queries = {

    1: () => {
      /* Log all the reviews by reader 1 */
      readerNodes[0].get("book_reviews").map().once(log);
    },

    2: () => {
      /* Log all the books reviewed by reader 1 */
      readerNodes[0].get("book_reviews").map().once(v => db.get(v.book).once(log));
    },

    3: () => {
      /* Log all the book titles reviewed by reader 1 */
      readerNodes[0].get("book_reviews").map().get("book").get("title").once(log);
    },

    4: () => {
      /* Given book 1, log its reviews: */
      bookNodes[0].get("reviews").map().once(log);
    },

    5: () => {
      /* Given book 1, log the 5-star reviews: */
      bookNodes[0].get("reviews")
      .map()
      .once(review => {
        if(review.rating === 5) {
          db.get(review.book).once(b => {
            log(review);
          });
        }
      });
    },

    6: () => {
      /* Given book 1, log the names of the readers who reviewed the book */
      bookNodes[0].get("reviews").map().get("reader").get("name").once(log);
    },

    7: () => {
      /* Log all the one-star reviews: */
      db.get("reviews/1").map().once(log);
    },

    8: () => {
      /* Given the one-star reviews, log the book titles: */
      db.get("reviews/1").map().get("book").get("title").once(log);
    },

    /* Given the one-star reviews, log the readers who left the reviews: */
    9:() => {
      db.get("reviews/1").map().get("reader").get("name").once(log);
    },

    /* Given one-star reviews, log the reader who left the reviews and the books */
    /*TODO: probably possible using promises as a structure ? */

    10: () => {
    },

    };

    return queries;
  }

  function reviewQueriesPromised(opt) {
    const {db, readerNodes, bookNodes} = opt;

    const queries = {
      1: () => {
        /* Given reader 1, log their book reviews */
        readerNodes[0].get("book_reviews").then()
        .then(o => filterMetadata(o))
        .then(refs => Promise.all(Object.keys(refs).map(k => db.get(k).then())))
        .then(r => r.map(v => filterMetadata(v)))
        .then(r => log(r));
      },

      2:() => {
        /* Log all the one-star rated books */
        db.get("reviews/1").then()
        .then(filterMetadata)
        .then(r => Promise.all(Object.keys(r).map(k => db.get(k).then())))
        .then(r => Promise.all(Object.keys(r).map(k => db.get(r[k].book["#"]).then())))
        .then(r => log(r));
      },

      3:() => {
        dbNodes({
          db,
          query: db.get("reviews/1")
        })
        .then(getAt(db, "book"))
        .then(log);
      }

    };

    return queries;
  }

  return {
    createNodes: createReviewBookNodes,
    queries: reviewBookQueries,
    queriesPromised: reviewQueriesPromised,
  }
};

